package build

import (
	"fmt"
	"encoding/hex"
	"crypto/sha1"
)


var (
	Version string
	User string
	Date string
	GitHash string
)


/* String */
func String() (string,bool) {

	if len(Date) == 0 {
		return "",false
	}

	
	return fmt.Sprintf("version %s, compiled by %s on %s",Version,User,Date),true 
}

func Hash() string {

	bs,ok := String()
	if !ok {
		return "-"
	}

	h := sha1.New()
	h.Write([]byte(bs))

	return hex.EncodeToString(h.Sum(nil))
}



/* when building a go binary, do the following :- 

 * go build -ldflags "-X mae.earth/pkg/build.Version={version} -X mae.earth/pkg/build.User={user} -X mae.earth/pkg/build.Date={date} -X mae.earth/pkg/build.GitHash='${hash}'" 
 */
